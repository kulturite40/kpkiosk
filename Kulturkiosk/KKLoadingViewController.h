//
//  KKLoadingViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 14.06.13.
//
//

#import <UIKit/UIKit.h>

@interface KKLoadingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *loadingMessage;
@property (weak, nonatomic) IBOutlet UILabel *pleaseWaitLabel;
-(void)loadedFile: (NSNotification*)notification;


@end
