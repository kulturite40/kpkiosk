//
//  KKLanguageChoiceCell.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 7/17/13.
//
//

#import "KKLanguageChoiceCell.h"

@interface KKLanguageChoiceCell ()

@end

@implementation KKLanguageChoiceCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
