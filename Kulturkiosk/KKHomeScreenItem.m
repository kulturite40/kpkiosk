//
//  KKHomeScreenItem.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/26/13.
//
//

#import "KKHomeScreenItem.h"

@implementation KKHomeScreenItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
