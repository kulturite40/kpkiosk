//
//  KKLanguageChoiceLayout.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 7/17/13.
//
//

#import <UIKit/UIKit.h>

@interface KKLanguageChoiceLayout : UICollectionViewFlowLayout

@end
