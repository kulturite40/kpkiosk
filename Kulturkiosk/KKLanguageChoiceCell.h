//
//  KKLanguageChoiceCell.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 7/17/13.
//
//

#import <UIKit/UIKit.h>

@interface KKLanguageChoiceCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *flagView;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
