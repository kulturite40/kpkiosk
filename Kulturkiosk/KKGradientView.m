//
//  KKGradientView.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/14/13.
//
//

#import "KKGradientView.h"
#import <QuartzCore/QuartzCore.h>

@implementation KKGradientView

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
    
    UIColor *grayColor = [UIColor colorWithRed:.262745098 green:.262745098 blue:.262745098 alpha:1.0];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[grayColor CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [self.layer insertSublayer:gradient atIndex:0];
  }
  return self;
}

@end
