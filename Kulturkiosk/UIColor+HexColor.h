//
//  UIColor+HexColor.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 12.06.13.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;
+ (UIColor *) colorWithHexColor: (NSString *) HexColor;

@end

