//
//  KKLoadingViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 14.06.13.
//
//

#import "KKLoadingViewController.h"

@implementation KKLoadingViewController

-(void)viewDidLoad
{
  self.loadingMessage.font=[UIFont fontWithName:@"MyriadPro-Regular" size:48];
  self.pleaseWaitLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
}

-(void)loadedFile: (NSNotification*)notification
{
 self.loadingMessage.text=[NSString stringWithFormat: @"Laster inn %@",[notification.userInfo objectForKey: @"fileName"]];
}

@end
