//
//  KKIndexViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 01.08.13.
//
//

#import <UIKit/UIKit.h>
#import "KKPresentationViewController.h"

@interface KKIndexViewController : KKPresentationViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIPageControl *pagerControl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *sortedRecords;

@end
