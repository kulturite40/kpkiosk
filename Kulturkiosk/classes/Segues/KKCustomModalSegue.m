//
//  KKModalSegue.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import "KKCustomModalSegue.h"

@implementation KKCustomModalSegue


-(void)perform{
  UIViewController *src = (UIViewController *) self.sourceViewController;
  UIViewController *dst = (UIViewController *) self.destinationViewController;
  [dst.view setFrame:CGRectMake(0, src.view.frame.size.height, src.view.frame.size.width, src.view.frame.size.height)];
  [UIView transitionWithView:src.view duration:2.0
                     options:UIViewAnimationOptionTransitionNone
                  animations:^{
                    
                    [src presentViewController:dst animated:NO completion:nil];
                    [dst.view setFrame:CGRectMake(src.view.frame.origin.x,src.view.frame.origin.y, src.view.frame.size.width, src.view.frame.size.height)];
                  }
                  completion:nil];

}

@end
