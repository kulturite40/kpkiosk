//
//  KKResourceCell.h
//  Kulturkiosk
//
//  Created by Rune Botten on 21.08.12.
//
//

#import <UIKit/UIKit.h>
#import "KKTopAlignImageView.h"

@interface KKResourceCell : UIView
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet KKTopAlignImageView *image;

@end
