//
//  KKImageCell.m
//  Kulturkiosk
//
//  Created by Rune Botten on 11.12.12.
//
//

#import "KKImageCell.h"

@implementation KKImageCell

-(BOOL) zoomEnabled
{
//  return self.imageView.zoomEnabled;
  return NO;
}

-(void) setZoomEnabled:(BOOL)zoomEnabled
{
//  self.imageView.zoomEnabled = zoomEnabled;
}

- (void)updateResource: (Resources*)res
{
  self.resource=res;
  UIImage *img=[UIImage imageWithContentsOfFile: [res imageWithFormat:KKFormatThumb].localUrl];
  UIImageView *thumbnail=[[UIImageView alloc] initWithImage:img];
  [self addSubview: thumbnail];
  
}


@end
