//
//  KKWarningView.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 18.01.13.
//
//

#import <UIKit/UIKit.h>

@interface KKWarningView : UIView
@property (weak, nonatomic) IBOutlet UILabel *warningTitle;
@property (weak, nonatomic) IBOutlet UILabel *warningMessage;

@end
