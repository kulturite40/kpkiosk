//
//  KKHelpViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/9/13.
//
//

#import "KKHelpViewController.h"


@interface KKHelpViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *helpView;

@end

@implementation KKHelpViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapScreen)];
  [self.view addGestureRecognizer:gestureRecognizer];
  
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  switch (self.helpType) {
    case KKHelpTypeSwipe:
    case KKHelpTypeSwipeLeft:
    case KKHelpTypeSwipeRight:
      [self.helpView setImage:[UIImage imageNamed:@"slide-horisontal.png"]];
      break;
    case KKHelpTypeZoom:
    case KKHelpTypePinchZoom:
      [self.helpView setImage:[UIImage imageNamed:@"pinch-zoom.png"]];
      break;
    case KKHelpTypeTap:
      [self.helpView setImage:[UIImage imageNamed:@"tap.png"]];
      break;
    default:
      break;
  }
}


- (void)didTapScreen
{
  [self.view removeFromSuperview];
  [self removeFromParentViewController];
}

@end
