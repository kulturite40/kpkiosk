#import "_Presentation.h"

typedef enum KKHelpType {
  KKHelpTypeSwipe = 0,
  KKHelpTypeSwipeLeft,
  KKHelpTypeSwipeRight,
  KKHelpTypePinchZoom,
  KKHelpTypeZoom,
  KKHelpTypeTap
  
} KKHelpType;

@interface Presentation : _Presentation {}
// Custom logic goes here.

- (NSArray*)files;
- (UIImage*)mapBackground;
- (Nodes*) node;
- (UIImage*) timelineBackground;
- (UIImage*) timelineInterLayer;
- (UIImage *) icon;
- (NSString*)className;
-(Content *)contentForLanguage:(KKLanguageType)lang;


@end
