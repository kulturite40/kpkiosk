//
//  KKObjectCache.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 13.07.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

@interface KKNodeObjectCache : NSObject <RKManagedObjectCaching >

@end
