#import "Nodes.h"
#import "Resources.h"
#import "Description.h"

#import <AVFoundation/AVAsset.h>

@implementation Nodes

// Different types of resources. Might get extended to include 'document' etc.
KKResourceType const KKResourceTypeImage = @"image";
KKResourceType const KKResourceTypeVideo = @"video";


-(NSArray*) childrenForLanguage:(KKLanguageType) lang
{
  NSMutableArray *array = [NSMutableArray array];
  for(Nodes *n in [self.children allObjects])
    if([n hasContentForLanguage:lang])
      [array addObject:n];
  return array;
}

-(NSArray*) pagesForLanguage:(KKLanguageType) lang
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_id == %@ AND type == 'page'", self];
  
  NSArray *nodes = [[Nodes objectsWithPredicate:predicate] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
  
  NSMutableArray *array = [NSMutableArray array];
  
  for(Nodes *n in nodes) {
    Content *c = [n contentForLanguage:lang];
    if(c)
      [array addObject:c];
  }
  return array;
}

// Custom logic goes here.
-(Content *)contentForLanguage:(KKLanguageType)lang
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"node == %@ AND language_id == %d", self, lang];
    NSArray *content = [Content objectsWithPredicate:predicate];
    if ([content count] == 0) {
        return nil;
    }
    return [content objectAtIndex:0];
}

-(BOOL) allChildrenHasContentForLanguage:(KKLanguageType) lang
{
  NSSet *childs = self.children;
  __block NSUInteger trueCount = 0;
  
  [childs enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
    Nodes *child = (Nodes*)obj;
    if([child hasContentForLanguage:lang])
      trueCount++;
  }];
  
  return trueCount == [childs count];
}

-(BOOL)hasContentForLanguage:(KKLanguageType)lang
{
    NSFetchRequest *fetch = [Content fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"node == %@ AND language_id == %d", self, lang];
    [fetch setPredicate:predicate];
    
    NSUInteger count = [Content countOfObjectsWithFetchRequest:fetch];
    
    if (count == 0) {
        return NO;
    }
    return YES;
}

-(BOOL)hasVideosForLanguage:(KKLanguageType)lang
{
  NSFetchRequest *fetch = [Description fetchRequest];
  
  NSPredicate *predicate;
  if([self nodeType] == KKTypeRecord) {
    predicate = [NSPredicate predicateWithFormat:@"resource.node.parent_id == %@ AND resource.type == %@ AND language_id == %d", self, KKResourceTypeVideo, lang];
  } else {
    predicate = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == %@ AND language_id == %d", self, KKResourceTypeVideo, lang];
  }
  
  [fetch setPredicate:predicate];
  return !![Description countOfObjectsWithFetchRequest:fetch];
}

-(BOOL)hasImagesForLanguage:(KKLanguageType)lang
{
  // All images are always available for all languages
  return YES;
  
//  NSFetchRequest *fetch = [Description fetchRequest];
//  
//  NSPredicate *predicate;
//  
//  if([self nodeType] == KKTypeRecord) {
//   predicate = [NSPredicate predicateWithFormat:@"resource.node.parent_id == %@ AND resource.type == %@ AND language_id == %d", self, KKResourceTypeImage, lang];
//  } else {
//   predicate = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == %@ AND language_id == %d", self, KKResourceTypeImage, lang];
//  }
//  
//  [fetch setPredicate:predicate];
//  return !![Description countOfObjectsWithFetchRequest:fetch];
}

-(KKType)nodeType
{
  
    if([self.type isEqualToString:@"record"])
    {
        return KKTypeRecord;
    } else if ([self.type isEqualToString:@"page"])
    {
        return KKTypePage;
    } else if([self.type isEqualToString:@"node"])
    {
        return KKTypeFolder;
    }
    return KKTypeUnknown;
}

-(NSArray*)files
{
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.node == %@", self];
  
  return [Files objectsWithPredicate:pred];
}

/*
 * This method returns all resources matching the type and language.
 *
 * type : 'image', 'video', 'document' etc...
 *
 */
-(NSArray*) resourcesWithType:(KKResourceType) type forLanguage:(KKLanguageType) lang
{
  
  // All images are available for all languages
  if(type == KKResourceTypeImage) {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"node == %@ AND type == 'image'", self];
    
    if ([self nodeType] == KKTypeRecord) {
      pred = [NSPredicate predicateWithFormat:@"node.parent_id == %@ AND type == 'image'", self];
    }
    
    return [Resources objectsWithPredicate:pred];
  }
  
  NSFetchRequest *fetch = [Description fetchRequest];
  
  NSPredicate *predicate;
  
  if([self nodeType] == KKTypeRecord) {
      predicate = [NSPredicate predicateWithFormat:@"resource.node.parent_id == %@ AND resource.type == %@ AND language_id == %d", self, type, lang];
  } else {
    predicate = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == %@ AND language_id == %d", self, type, lang];
  }
  
  [fetch setPredicate:predicate];
  
  NSArray *descs = [Description objectsWithFetchRequest:fetch];
  // Collect the resources
  NSMutableArray *res = [[NSMutableArray alloc] initWithCapacity:[descs count]];
  for(Description *d in descs) {
    [res addObject:d.resource];
  }
  
  if(type == KKResourceTypeVideo) {
    // Filter unwatchable videos
    NSMutableArray *videos = [NSMutableArray array];
    for(Resources *r in res) {
      if(![r.files count]) continue;
      Files *video = [r.files allObjects][0];
      if (video.localUrl) {
        NSURL *url = [NSURL fileURLWithPath:video.localUrl];
        AVURLAsset *asset = [AVURLAsset assetWithURL:url];
        if(asset.playable)
          [videos addObject:r];

      }
    }
    res = videos;
  }
  
  return res;
}


// Only used for the root node atm
-(UIImage*) backgroundImage
{
  UIImage *image = nil;
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  if ([self nodeType] == KKTypeFolder) {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == 'bg_image' AND type == %@", self, strFormat];
    NSArray *files = [Files objectsWithPredicate:pred];
    if([files count]) {
      Files *f = [files objectAtIndex:0];
      image = [UIImage imageWithContentsOfFile:f.localUrl];
    }
  }
  
  return image;
}

// Getting the main image for folders and records

-(UIImage*) thumbnail
{
  UIImage *image = nil;
  NSString* strFormat = [Nodes stringForImageFormat:KKFormatThumb];
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == 'image' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    image = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  return image;
}

-(UIImage*) mainImage
{
  UIImage *image = nil;
  NSString* strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == 'image' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    image = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  return image;
}

-(UIImage*) headerImage
{
  UIImage *image = nil;
  NSString* strFormat = [Nodes stringForImageFormat:KKFormatHeader];
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == 'image' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    image = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  return image;
}

-(NSArray*)imagesWithType:(KKImageFormat)format
{
  NSString *strFormat = [Nodes stringForImageFormat:format];
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == 'image' AND type == %@", self, strFormat];

  if ([self nodeType] == KKTypeRecord) {
    pred = [NSPredicate predicateWithFormat:@"resource.node.parent_id == %@ AND resource.type == 'image' AND type == %@", self, strFormat];
  }
  
  return [Files objectsWithPredicate:pred];
}

+(NSString*)stringForImageFormat:(KKImageFormat)format
{
  NSString *str = @"original";
  switch (format) {
    case KKFormatHeader:
      str = @"header";
      break;
    case KKFormatMain:
      str = @"main";
      break;
    case KKFormatOriginal:
      str = @"original";
      break;
    case KKFormatSmall:
      str = @"small";
      break;
    case KKFormatThumb:
      str = @"thumb";
    default:
      break;
  }
  return str;
}

+(Nodes*) rootNode
{
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"(parent_id.@count == 0)"];
  NSArray *objects = [Nodes objectsWithPredicate:pred];
  if([objects count])
    return [objects objectAtIndex:0];
  return nil;
}

@end
