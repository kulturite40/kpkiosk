// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Files.h instead.

#import <CoreData/CoreData.h>


extern const struct FilesAttributes {
	__unsafe_unretained NSString *localUrl;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *url;
} FilesAttributes;

extern const struct FilesRelationships {
	__unsafe_unretained NSString *resource;
} FilesRelationships;

extern const struct FilesFetchedProperties {
} FilesFetchedProperties;

@class Resources;





@interface FilesID : NSManagedObjectID {}
@end

@interface _Files : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FilesID*)objectID;





@property (nonatomic, strong) NSString* localUrl;



//- (BOOL)validateLocalUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Resources *resource;

//- (BOOL)validateResource:(id*)value_ error:(NSError**)error_;





@end

@interface _Files (CoreDataGeneratedAccessors)

@end

@interface _Files (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveLocalUrl;
- (void)setPrimitiveLocalUrl:(NSString*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (Resources*)primitiveResource;
- (void)setPrimitiveResource:(Resources*)value;


@end
