// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Content.m instead.

#import "_Content.h"

const struct ContentAttributes ContentAttributes = {
	.body = @"body",
	.created_at = @"created_at",
	.desc = @"desc",
	.identifier = @"identifier",
	.ingress = @"ingress",
	.language_code = @"language_code",
	.language_id = @"language_id",
	.language_title = @"language_title",
	.position = @"position",
	.subtitle = @"subtitle",
	.title = @"title",
	.updated_at = @"updated_at",
};

const struct ContentRelationships ContentRelationships = {
	.links = @"links",
	.node = @"node",
	.presentation = @"presentation",
};

const struct ContentFetchedProperties ContentFetchedProperties = {
};

@implementation ContentID
@end

@implementation _Content

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Content" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Content";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Content" inManagedObjectContext:moc_];
}

- (ContentID*)objectID {
	return (ContentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"language_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"language_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic body;






@dynamic created_at;






@dynamic desc;






@dynamic identifier;



- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithShort:value_]];
}





@dynamic ingress;






@dynamic language_code;






@dynamic language_id;



- (int16_t)language_idValue {
	NSNumber *result = [self language_id];
	return [result shortValue];
}

- (void)setLanguage_idValue:(int16_t)value_ {
	[self setLanguage_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLanguage_idValue {
	NSNumber *result = [self primitiveLanguage_id];
	return [result shortValue];
}

- (void)setPrimitiveLanguage_idValue:(int16_t)value_ {
	[self setPrimitiveLanguage_id:[NSNumber numberWithShort:value_]];
}





@dynamic language_title;






@dynamic position;



- (int16_t)positionValue {
	NSNumber *result = [self position];
	return [result shortValue];
}

- (void)setPositionValue:(int16_t)value_ {
	[self setPosition:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result shortValue];
}

- (void)setPrimitivePositionValue:(int16_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithShort:value_]];
}





@dynamic subtitle;






@dynamic title;






@dynamic updated_at;






@dynamic links;

	
- (NSMutableSet*)linksSet {
	[self willAccessValueForKey:@"links"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"links"];
  
	[self didAccessValueForKey:@"links"];
	return result;
}
	

@dynamic node;

	

@dynamic presentation;

	






@end
