// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Resources.h instead.

#import <CoreData/CoreData.h>


extern const struct ResourcesAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *page_id;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *type;
} ResourcesAttributes;

extern const struct ResourcesRelationships {
	__unsafe_unretained NSString *credits;
	__unsafe_unretained NSString *descs;
	__unsafe_unretained NSString *device;
	__unsafe_unretained NSString *files;
	__unsafe_unretained NSString *node;
	__unsafe_unretained NSString *presentation;
} ResourcesRelationships;

extern const struct ResourcesFetchedProperties {
} ResourcesFetchedProperties;

@class Credit;
@class Description;
@class Device;
@class Files;
@class Nodes;
@class Presentation;







@interface ResourcesID : NSManagedObjectID {}
@end

@interface _Resources : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ResourcesID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* page_id;



@property int16_t page_idValue;
- (int16_t)page_idValue;
- (void)setPage_idValue:(int16_t)value_;

//- (BOOL)validatePage_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int16_t positionValue;
- (int16_t)positionValue;
- (void)setPositionValue:(int16_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *credits;

- (NSMutableSet*)creditsSet;




@property (nonatomic, strong) NSSet *descs;

- (NSMutableSet*)descsSet;




@property (nonatomic, strong) Device *device;

//- (BOOL)validateDevice:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *files;

- (NSMutableSet*)filesSet;




@property (nonatomic, strong) Nodes *node;

//- (BOOL)validateNode:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Presentation *presentation;

//- (BOOL)validatePresentation:(id*)value_ error:(NSError**)error_;





@end

@interface _Resources (CoreDataGeneratedAccessors)

- (void)addCredits:(NSSet*)value_;
- (void)removeCredits:(NSSet*)value_;
- (void)addCreditsObject:(Credit*)value_;
- (void)removeCreditsObject:(Credit*)value_;

- (void)addDescs:(NSSet*)value_;
- (void)removeDescs:(NSSet*)value_;
- (void)addDescsObject:(Description*)value_;
- (void)removeDescsObject:(Description*)value_;

- (void)addFiles:(NSSet*)value_;
- (void)removeFiles:(NSSet*)value_;
- (void)addFilesObject:(Files*)value_;
- (void)removeFilesObject:(Files*)value_;

@end

@interface _Resources (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSNumber*)primitivePage_id;
- (void)setPrimitivePage_id:(NSNumber*)value;

- (int16_t)primitivePage_idValue;
- (void)setPrimitivePage_idValue:(int16_t)value_;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int16_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int16_t)value_;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveCredits;
- (void)setPrimitiveCredits:(NSMutableSet*)value;



- (NSMutableSet*)primitiveDescs;
- (void)setPrimitiveDescs:(NSMutableSet*)value;



- (Device*)primitiveDevice;
- (void)setPrimitiveDevice:(Device*)value;



- (NSMutableSet*)primitiveFiles;
- (void)setPrimitiveFiles:(NSMutableSet*)value;



- (Nodes*)primitiveNode;
- (void)setPrimitiveNode:(Nodes*)value;



- (Presentation*)primitivePresentation;
- (void)setPrimitivePresentation:(Presentation*)value;


@end
