#import "_Setting.h"

@interface Setting : _Setting {}
+ (NSString *) valueForKey:(NSString*) key;
@end
