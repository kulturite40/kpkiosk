#import "Resources.h"

@interface Resources()

@property (strong) NSString *titles;
@property (strong) NSString *descriptions;
@property (strong) NSString *creditsString;

@end

@implementation Resources

@synthesize titles,descriptions,creditsString;

// Custom logic goes here.
-(Files*)imageWithFormat:(KKImageFormat)format
{
  NSString *strFormat = [Nodes stringForImageFormat:format];
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource == %@ AND resource.type == %@ AND type == %@", self, KKResourceTypeImage, strFormat];
  
  NSArray *files = [Files objectsWithPredicate:pred];
  if ([files count] != 0) {
    return [files objectAtIndex:0];
  }
  
  return nil;
}

-(NSArray*) descriptionsForLanguage:(KKLanguageType) lang
{
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource == %@ AND language_id == %d", self, lang];
 return [Description objectsWithPredicate:pred];
}

-(NSString*) creditsForLanguage: (KKLanguageType) lang
{
  if(!self.creditsString) {
    [self populateStrings: lang];
  }
  return  self.creditsString;
}


-(NSString*) titleForLanguage: (KKLanguageType) lang
{
  if(!self.titles) {
    [self populateStrings: lang];
  }
  return self.titles;
}


-(NSString*) descriptionForLanguage: (KKLanguageType) lang
{
  if(!self.descriptions) {
    [self populateStrings: lang];
  }
  return self.descriptions;
}

- (void)populateStrings: (KKLanguageType)lang {
  self.descriptions=@"";
  self.titles=@"";
  self.creditsString=@"";
  NSArray *descs = [self descriptionsForLanguage:lang];
  for(Description *d in descs) {
    if([d.desc length]) {
      self.descriptions = [self.descriptions stringByAppendingFormat:@"%@\n", d.desc];
    }
    if([d.title length]) {
      self.titles= [self.titles stringByAppendingFormat: @"%@\n",d.title];
    }
  }
  for(Credit *c in self.credits) {
    NSString *credit = [c localizedCredit];
    if(self.creditsString)
      self.creditsString = [self.creditsString stringByAppendingFormat:@"%@\n", credit];
  }
}


-(NSNumber *)page_id
{
  [self willAccessValueForKey:@"page_id"];
  NSNumber *pageId = self.node.page_id;
  [self didAccessValueForKey:@"page_id"];
  
  return pageId;
}

@end
