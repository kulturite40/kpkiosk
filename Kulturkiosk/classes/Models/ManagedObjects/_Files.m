// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Files.m instead.

#import "_Files.h"

const struct FilesAttributes FilesAttributes = {
	.localUrl = @"localUrl",
	.type = @"type",
	.url = @"url",
};

const struct FilesRelationships FilesRelationships = {
	.resource = @"resource",
};

const struct FilesFetchedProperties FilesFetchedProperties = {
};

@implementation FilesID
@end

@implementation _Files

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Files" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Files";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Files" inManagedObjectContext:moc_];
}

- (FilesID*)objectID {
	return (FilesID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic localUrl;






@dynamic type;






@dynamic url;






@dynamic resource;

	






@end
