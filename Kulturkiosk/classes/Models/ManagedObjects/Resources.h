#import "_Resources.h"
#import "Nodes.h"
#import "Files.h"
#import "Description.h"

@interface Resources : _Resources {}
// Custom logic goes here.

-(Files*)imageWithFormat:(KKImageFormat)format;
-(NSArray*) descriptionsForLanguage:(KKLanguageType) lang;
-(NSString*) descriptionForLanguage: (KKLanguageType) lang;
-(NSString*) titleForLanguage: (KKLanguageType) lang;
-(NSString*) creditsForLanguage: (KKLanguageType) lang;
@end
