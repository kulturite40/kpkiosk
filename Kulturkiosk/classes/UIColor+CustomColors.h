//
//  UIColor+CustomColors.h
//  Kulturkiosk
//
//  Created by Rune Botten on 18.12.12.
//
//

#import <Foundation/Foundation.h>

@interface UIColor(CustomColors)
+(UIColor*) selectedColor;
@end
