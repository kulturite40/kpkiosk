//
//  KKImagePresentationViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 05.11.13.
//
//

#import "KKIndexViewController.h"
#import "Record.h"

@interface KKImagePresentationViewController : KKPresentationViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic) Nodes *node;

- (void)loadImage: (Resources*)image;
- (IBAction)dismissController:(id)sender;


@end
