//
//  KKVideoThumbCell.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 11.11.13.
//
//

#import "KKVideoThumbCell.h"

@implementation KKVideoThumbCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setThumbnail: (UIImage*)thumb;
{
  UIImageView *thumbnail = [[UIImageView alloc] initWithImage:thumb];
  CGRect frame=self.frame;
  frame.origin.x=frame.origin.y=0;
  thumbnail.frame=frame;
  thumbnail.contentMode=UIViewContentModeScaleToFill;
  [self addSubview: thumbnail];
}

@end
