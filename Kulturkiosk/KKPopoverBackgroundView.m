//
//  KKPopoverBackgroundView.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/8/13.
//
//

#import "KKPopoverBackgroundView.h"
#import <QuartzCore/QuartzCore.h>

#define ARROW_BASE 38
#define ARROW_HEIGHT 19.0

@interface KKPopoverBackgroundView () {
  UIImageView *_borderImageView;
  UIImageView *_arrowView;
  CGFloat _arrowOffset;
  UIPopoverArrowDirection _arrowDirection;
}

@end

@implementation KKPopoverBackgroundView

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    
    self.backgroundColor = [UIColor clearColor];
    _borderImageView.backgroundColor = [UIColor clearColor];
    _arrowView.backgroundColor = [UIColor clearColor];
    
    _arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popover-arrow.png"]];
    _arrowView.layer.shadowRadius = 0;
    _arrowView.layer.shadowOpacity = 0;

    [self addSubview:_arrowView];
    
  }
  
  return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)layoutSubviews{
  
  
  CGFloat _height = self.frame.size.height;
  CGFloat _width = self.frame.size.width;
  CGFloat _left = 0.0;
  CGFloat _top = 0.0;
  CGFloat _coordinate = 0.0;
  CGAffineTransform _rotation = CGAffineTransformIdentity;
  
  
  switch (self.arrowDirection) {
    case UIPopoverArrowDirectionUp:
      _top += ARROW_HEIGHT;
      _height -= ARROW_HEIGHT;
      _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
      _arrowView.frame = CGRectMake(_coordinate, 0, ARROW_BASE, ARROW_HEIGHT);
      break;
      
      
    case UIPopoverArrowDirectionDown:
      _height -= ARROW_HEIGHT;
      _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
      _arrowView.frame = CGRectMake(_coordinate, _height, ARROW_BASE, ARROW_HEIGHT);
      _rotation = CGAffineTransformMakeRotation( M_PI );
      break;
      
    case UIPopoverArrowDirectionLeft:
      _left += ARROW_HEIGHT;
      _width -= ARROW_HEIGHT;
      _coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (ARROW_BASE/2);
      _arrowView.frame = CGRectMake(0, _coordinate, ARROW_BASE, ARROW_HEIGHT);
      _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
      break;
      
    case UIPopoverArrowDirectionRight:
      _width -= ARROW_HEIGHT;
      _coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (ARROW_HEIGHT/2);
      _arrowView.frame = CGRectMake(_width, _coordinate, ARROW_BASE, ARROW_HEIGHT);
      _rotation = CGAffineTransformMakeRotation( M_PI_2 );
      
      break;
    case UIPopoverArrowDirectionAny:
    case UIPopoverArrowDirectionUnknown:
      break;
  }
  
  _borderImageView.frame =  CGRectMake(_left , _top, _width, _height);
  
  
  [_arrowView setTransform:_rotation];
}

+ (UIEdgeInsets)contentViewInsets
{
  return UIEdgeInsetsZero;
}

- (CGFloat) arrowOffset {
  return _arrowOffset;
}

- (void) setArrowOffset:(CGFloat)arrowOffset {
  _arrowOffset = arrowOffset;
}

- (UIPopoverArrowDirection)arrowDirection {
  return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
  _arrowDirection = arrowDirection;
}

+(CGFloat)arrowHeight {
  return ARROW_HEIGHT;
}

+(CGFloat)arrowBase{
  return ARROW_BASE;
}

+ (BOOL)wantsDefaultContentAppearance
{
  return NO;
}

@end
