//
//  KKAppDelegate.h
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import <RestKit/NSManagedObjectContext+RKAdditions.h>
#import "Nodes.h"
#import "Description.h"
#import "Resources.h"
#import "Device.h"
#import "Content.h"
#import "Record.h"
#import "Presentation.h"
#import "Setting.h"
#import "Credit.h"
#import "Files.h"
#import "Links.h"
#import "KKAssetsManager.h"

@interface KKAppDelegate : UIResponder <UIApplicationDelegate, RKObjectLoaderDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL shouldRegisterTouch;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;

- (void)stopTimer;
- (void)startTimer;

@end
