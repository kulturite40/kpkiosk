//
//  KKImagePresentationViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 21.11.13.
//
//

#import "KKImagePresentationViewController.h"
#import "KKImageCell.h"


@interface KKImagePresentationViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIView *imageContainer;
@property (weak, nonatomic) IBOutlet UILabel *imagetitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *imageDescriptionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *imageThumbs;
@property (strong,nonatomic) NSArray *images;
@property (strong,nonatomic) UIWindow *externalWindow;

@end

@implementation KKImagePresentationViewController



- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
  [self.descriptionLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:20]];
  self.descriptionLabel.numberOfLines=0;
  [self.imagetitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24]];
  [self.imageDescriptionLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:20]];
  self.imageDescriptionLabel.numberOfLines=0;
  
  self.images=[[self.node resourcesWithType:KKResourceTypeImage forLanguage:[KKLanguage currentLanguage]]
               sortedArrayUsingDescriptors:@[
                                             [NSSortDescriptor sortDescriptorWithKey:@"page_id" ascending:YES],
                                             [NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES]]];

  
  // Do any additional setup after loading the view.
  if([self.images count]) {
    [self loadImage: [self.images firstObject]];
  }
  Content *content=[self.node contentForLanguage:[KKLanguage currentLanguage]];
  self.titleLabel.text=content.title;
  self.descriptionLabel.text=content.desc;
  [self.descriptionLabel sizeToFit];
}



- (void)loadImage: (Resources*)image {
  self.imagetitleLabel.text=[image titleForLanguage: [KKLanguage currentLanguage]];
  self.imageDescriptionLabel.text=[[image descriptionForLanguage: [KKLanguage currentLanguage]] stringByAppendingString: [image creditsForLanguage:[KKLanguage currentLanguage]]];
  for (UIView *view  in self.imageContainer.subviews) {
    [view removeFromSuperview];
  }
  UIImage *resourceImage=[UIImage imageWithContentsOfFile:[image imageWithFormat: KKFormatSmall].localUrl];
  UIImageView *imgView=[[UIImageView alloc] initWithImage:
                        resourceImage];
  CGRect frame=self.imageContainer.frame;
  frame.origin.x=frame.origin.y=0;
  imgView.frame=frame;
  imgView.contentMode=UIViewContentModeScaleAspectFit;
  [self.imageContainer addSubview: imgView];
  if([[UIScreen screens]count] > 1 ) {
    UIScreen *externalDisplay=[[UIScreen screens] objectAtIndex:1];
    CGRect bounds=externalDisplay.bounds;
    self.externalWindow = [[UIWindow alloc] initWithFrame: bounds];
    self.externalWindow.screen=externalDisplay;
    self.externalWindow.hidden=NO;
    UIImageView *externalImg=[[UIImageView alloc] initWithImage: resourceImage ];
    externalImg.frame=self.externalWindow.frame;
    externalImg.contentMode=UIViewContentModeScaleAspectFit;
    [self.externalWindow addSubview: externalImg];
  }
  //  [self.imageDescriptionLabel sizeToFit];
}



- (IBAction)dismissController:(id)sender {
    self.externalWindow.hidden=YES;
    [self dismissViewControllerAnimated:NO completion:^{
      
    }];
  }


#pragma mark - Collection view for bottom

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"thumbCell" forIndexPath:indexPath];
  [cell updateResource: [self.images objectAtIndex:indexPath.row]];
  return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  [self loadImage: [self.images objectAtIndex:indexPath.row]];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return [self.images count];
}

@end
